///<reference path="typings/jquery/jquery.d.ts" />
///<reference path="typings/bootstrap/bootstrap.d.ts" />
///<reference path="typings/bootstrap-slider/bootstrap-slider.d.ts" />

var taxMultiplier:number = 0.19;

function roundUp(value:number, places:number) {
  var pow = Math.pow(10, places);
  return (Math.ceil(value * pow) / pow);
}

function roundDown(value:number, places:number){
  var pow = Math.pow(10, places);
  return (Math.floor(value * pow) / pow);
}

function round(value:number, places:number){
  var pow = Math.pow(10, places);
  return (Math.round(value * pow) / pow);
}

class Point {
  percent: number;
  amount: number;
  days: number;
  private interest: number;
  private tax: number;
  sum: number;

  constructor(percent:number, amount:number, days:number = 1){
    this.percent = percent;
    this.amount = amount;
    this.days = days;
  }

  private CountInterest(): void {
    this.interest = round((((this.percent * this.amount) / 365)*this.days), 2);
  }

  private CountTax(): void {
    this.tax = roundUp(this.interest * taxMultiplier, 2);
  }

  CountValue(): void {
    this.CountInterest();
    this.CountTax();
    this.sum = round((this.interest - this.tax), 2);
  }

  Render(): string{
    this.CountValue();

    var output:string = '<tr>';
      output += '<td>' + this.amount + '</td>';
      output += '<td>' + this.percent*100 + '</td>';
      output += '<td>' + this.interest + '</td>';
      output += '<td>' + this.tax + '</td>';
      output += '<td>' + this.sum + '</td>';
      output += '</tr>';
    return output;
  }
};

class Container {
  private items: Point[] = [];

  Add(p:Point): void{
    this.items.push(p);
  }

  Clear(): void{
    this.items = [];
  }

  GeneratePoints(percent:number, startPoint:number, endPoint:number): void{
    var tmp:Point;
    var tmpValue = 0;

    for(var i=startPoint; i<endPoint; i++){
      tmp = new Point(percent, i);
      tmp.CountValue();
      if(tmp.sum > tmpValue){
        this.items.push(tmp);
        tmpValue = tmp.sum;
      }
    }
  }

  Render(): string {
    var output:string = '<table class="table table-striped">';
      output += '<thead><tr>';
      output += '<th>Kwota [zł]</th>';
      output += '<th>Procent [%]</th>';
      output += '<th>Odestki [zł]</th>';
      output += '<th>Podatek [zł]</th>';
      output += '<th>Suma [zł]</th>';
      output += '</tr></thead>';
      output += '<tbody>';

    this.items.forEach(item => {
      output += item.Render();
    });

      output += '</tbody>';
      output += '</table>';
    return output;
  }
};

$('documemnt').ready(function(){

  $('#menu-nav-tabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

  $('#rangeMultiple').slider({ min: 1, max: 10000, value: [1,10000], focus: true});

  var container: Container = new Container();
  $('#outputCountTables').html(container.Render());

  $('#countButton').on('click', function() {
    var $btn = $(this).button('loading');

    var percent = $('#percent').val()/100;
    var amount = $('#price').val();
    var days = $('#days').val();

    if(percent != 0 && amount != "" && days > 0)
      container.Add(new Point(percent, amount, days));
    $('#outputCountTables').html(container.Render());

    setTimeout(function() { $btn.button('reset'); }, 500);
  })
    
  $('.clearButton').on('click', function() {
    container.Clear();
    $('#outputCountTables').html(container.Render());
  });

  $('#criticalPathButton').on('click', function() {
    var $btn = $(this).button('loading');

    var percent = $('#percentMultiple').val() / 100;
    if (percent == 0){
      $btn.button('reset');
      return;
    }

    container.Clear();
    container.GeneratePoints(percent, parseInt($('#rangeMultiple').val().split(',')[0]), parseInt($('#rangeMultiple').val().split(',')[1]));
    $('#outputCountTables').html(container.Render());

    setTimeout(function() { $btn.button('reset'); }, 500);
  });
})
