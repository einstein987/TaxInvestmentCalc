///<reference path="typings/jquery/jquery.d.ts" />
///<reference path="typings/bootstrap/bootstrap.d.ts" />
///<reference path="typings/bootstrap-slider/bootstrap-slider.d.ts" />
var taxMultiplier = 0.19;
function roundUp(value, places) {
    var pow = Math.pow(10, places);
    return (Math.ceil(value * pow) / pow);
}
function roundDown(value, places) {
    var pow = Math.pow(10, places);
    return (Math.floor(value * pow) / pow);
}
function round(value, places) {
    var pow = Math.pow(10, places);
    return (Math.round(value * pow) / pow);
}
var Point = (function () {
    function Point(percent, amount, days) {
        if (days === void 0) { days = 1; }
        this.percent = percent;
        this.amount = amount;
        this.days = days;
    }
    Point.prototype.CountInterest = function () {
        this.interest = round((((this.percent * this.amount) / 365) * this.days), 2);
    };
    Point.prototype.CountTax = function () {
        this.tax = roundUp(this.interest * taxMultiplier, 2);
    };
    Point.prototype.CountValue = function () {
        this.CountInterest();
        this.CountTax();
        this.sum = round((this.interest - this.tax), 2);
    };
    Point.prototype.Render = function () {
        this.CountValue();
        var output = '<tr>';
        output += '<td>' + this.amount + '</td>';
        output += '<td>' + this.percent * 100 + '</td>';
        output += '<td>' + this.interest + '</td>';
        output += '<td>' + this.tax + '</td>';
        output += '<td>' + this.sum + '</td>';
        output += '</tr>';
        return output;
    };
    return Point;
})();
;
var Container = (function () {
    function Container() {
        this.items = [];
    }
    Container.prototype.Add = function (p) {
        this.items.push(p);
    };
    Container.prototype.Clear = function () {
        this.items = [];
    };
    Container.prototype.GeneratePoints = function (percent, startPoint, endPoint) {
        var tmp;
        var tmpValue = 0;
        for (var i = startPoint; i < endPoint; i++) {
            tmp = new Point(percent, i);
            tmp.CountValue();
            if (tmp.sum > tmpValue) {
                this.items.push(tmp);
                tmpValue = tmp.sum;
            }
        }
    };
    Container.prototype.Render = function () {
        var output = '<table class="table table-striped">';
        output += '<thead><tr>';
        output += '<th>Kwota [zł]</th>';
        output += '<th>Procent [%]</th>';
        output += '<th>Odestki [zł]</th>';
        output += '<th>Podatek [zł]</th>';
        output += '<th>Suma [zł]</th>';
        output += '</tr></thead>';
        output += '<tbody>';
        this.items.forEach(function (item) {
            output += item.Render();
        });
        output += '</tbody>';
        output += '</table>';
        return output;
    };
    return Container;
})();
;
$('documemnt').ready(function () {
    $('#menu-nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $('#rangeMultiple').slider({ min: 1, max: 10000, value: [1, 10000], focus: true });
    var container = new Container();
    $('#outputCountTables').html(container.Render());
    $('#countButton').on('click', function () {
        var $btn = $(this).button('loading');
        var percent = $('#percent').val() / 100;
        var amount = $('#price').val();
        var days = $('#days').val();
        if (percent != 0 && amount != "" && days > 0)
            container.Add(new Point(percent, amount, days));
        $('#outputCountTables').html(container.Render());
        setTimeout(function () {
            $btn.button('reset');
        }, 500);
    });
    $('.clearButton').on('click', function () {
        container.Clear();
        $('#outputCountTables').html(container.Render());
    });
    $('#criticalPathButton').on('click', function () {
        var $btn = $(this).button('loading');
        var percent = $('#percentMultiple').val() / 100;
        if (percent == 0) {
            $btn.button('reset');
            return;
        }
        container.Clear();
        container.GeneratePoints(percent, parseInt($('#rangeMultiple').val().split(',')[0]), parseInt($('#rangeMultiple').val().split(',')[1]));
        $('#outputCountTables').html(container.Render());
        setTimeout(function () {
            $btn.button('reset');
        }, 500);
    });
});
